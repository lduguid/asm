.PHONY:	clean

runme:	main.o asm.o
	g++ -o runme main.o asm.o	

main.o:	main.cpp
	g++ -c main.cpp

asm.o:	asm.asm
	nasm -f elf64 asm.asm -o asm.o
clean:
	rm -f runme main.o asm.o
